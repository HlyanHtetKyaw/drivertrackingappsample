package com.example.codetestforhaulio.jobs

import Geolocation
import JobListResponse
import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.codetestforhaulio.R
import com.example.codetestforhaulio.detail.DetailsActivity


class JobListAdapter(private val activity: Activity, private val mItems: List<JobListResponse>
                     , private val userName: String, private val photoUrl: String) : RecyclerView.Adapter<JobListAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.job_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val job = mItems[position]
        holder.tv_job_no.text = "Job Number: " + job.job_id
        holder.tv_address.text = "Address: " + job.address
        holder.tv_company.text = "Company:" + job.company
        holder.rl_item.setOnClickListener { goToDetail(job.job_id, job.geolocation) }
        holder.iv_accept.setOnClickListener { goToDetail(job.job_id, job.geolocation) }
    }

    override fun getItemCount(): Int {
        return mItems.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var tv_job_no: TextView = itemView.findViewById(R.id.tv_job_no)
        internal var tv_company: TextView = itemView.findViewById(R.id.tv_company)
        internal var tv_address: TextView = itemView.findViewById(R.id.tv_address)
        internal var rl_item: RelativeLayout = itemView.findViewById(R.id.rl_item)
        internal var iv_accept: Button = itemView.findViewById(R.id.iv_accept)
    }

    private fun goToDetail(job_id: Int, location: Geolocation) {
        val intent = Intent(activity, DetailsActivity::class.java)
        intent.putExtra("ussername", userName)
        intent.putExtra("photoUrl", photoUrl)
        intent.putExtra("job_id", job_id)
        intent.putExtra("lat", location.latitude)
        intent.putExtra("lng", location.longitude)
        activity.startActivity(intent)
    }
}
