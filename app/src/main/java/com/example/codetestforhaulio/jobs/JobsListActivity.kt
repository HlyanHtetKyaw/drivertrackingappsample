package com.example.codetestforhaulio.jobs

import JobListResponse
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatImageView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.codetestforhaulio.R
import com.example.codetestforhaulio.data.network.DataManager
import com.example.codetestforhaulio.signIn.SignInWithGoogleActivity
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions


class JobsListActivity : AppCompatActivity() {

    private var jobListAdapter: JobListAdapter? = null
    private var recyclerView: RecyclerView? = null
    private var progressBar: ProgressBar? = null
    private var btn_sign_out: AppCompatImageView? = null
    private var mGoogleSignInClient: GoogleSignInClient? = null
    private var userName = ""
    private var photoUrl = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_jobs_list)

        recyclerView = findViewById(R.id.recyclerView)
        progressBar = findViewById(R.id.progress_bar)
        btn_sign_out = findViewById(R.id.btn_sign_out)
        btn_sign_out!!.setOnClickListener { signOut() }
        val bundle = intent.extras
        if (null != bundle) {
            userName = bundle.getString("name")!!
            photoUrl = bundle.getString("photoUrl")!!
        }

        val llm = LinearLayoutManager(this)
        recyclerView!!.layoutManager = llm

        val viewModel = createViewModel()
        viewModel.loadingStatus.observe(this, LoadingObserver())
        viewModel.jobs.observe(this, JobListObserver())
        viewModel.loadJobList()

    }

    private fun createViewModel(): JobsListViewModel {
        val factory = JobListViewModelFactory(DataManager.instance.jobListService)
        return ViewModelProviders.of(this, factory).get(JobsListViewModel::class.java)
    }

    private inner class JobListObserver : Observer<List<JobListResponse>> {
        override fun onChanged(jobs: List<JobListResponse>?) {
            if (jobs == null) return

            jobListAdapter = JobListAdapter(this@JobsListActivity, jobs, userName, photoUrl)
            recyclerView!!.adapter = jobListAdapter
        }
    }

    //Observer
    private inner class LoadingObserver : Observer<Boolean> {

        override fun onChanged(isLoading: Boolean?) {
            if (isLoading == null) return

            if (isLoading) {
                progressBar!!.visibility = View.VISIBLE
            } else {
                progressBar!!.visibility = View.GONE
            }
        }
    }

    private fun signOut() {
        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build()
        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso)
        mGoogleSignInClient!!.signOut()
                .addOnCompleteListener(this) {
                    startActivity(Intent(this, SignInWithGoogleActivity::class.java))
                }

    }
}
