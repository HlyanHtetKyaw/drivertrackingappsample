package com.example.codetestforhaulio.jobs

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.codetestforhaulio.data.network.service.JobListService


class JobListViewModelFactory(private val jobListService: JobListService) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(JobsListViewModel::class.java)) {
            return JobsListViewModel(jobListService) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }

}
