package com.example.codetestforhaulio.jobs

import JobListResponse
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.codetestforhaulio.data.network.service.JobListService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class JobsListViewModel(private val jobListServece: JobListService) : ViewModel() {

    val jobs: MutableLiveData<List<JobListResponse>> = MutableLiveData()
    internal val loadingStatus: MutableLiveData<Boolean> = MutableLiveData()

    fun loadJobList() {
        setIsLoading(true)
        val jobListCall = jobListServece.jobListApi.getAllJobs()
        jobListCall.enqueue(JobListCallBack())
    }

    private fun setIsLoading(loading: Boolean) {
        loadingStatus.postValue(loading)
    }

    /**
     * Callback
     */
    private inner class JobListCallBack : Callback<List<JobListResponse>> {

        override fun onResponse(call: Call<List<JobListResponse>>, response: Response<List<JobListResponse>>) {
            val jobListResponse = response.body()

            if (jobListResponse != null) {
                setJobsList(jobListResponse)
            } else {
                setJobsList(emptyList())
            }
        }

        override fun onFailure(call: Call<List<JobListResponse>>, t: Throwable) {
            setJobsList(emptyList())

        }
    }

    private fun setJobsList(jobList: List<JobListResponse>) {
        setIsLoading(false)
        jobs.postValue(jobList)
    }

}
