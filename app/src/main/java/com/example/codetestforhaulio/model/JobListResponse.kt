import com.google.gson.annotations.SerializedName


data class JobListResponse(

        @SerializedName("id") val id: Int,
        @SerializedName("job-id") val job_id: Int,
        @SerializedName("priority") val priority: Int,
        @SerializedName("company") val company: String,
        @SerializedName("address") val address: String,
        @SerializedName("geolocation") val geolocation: Geolocation
)