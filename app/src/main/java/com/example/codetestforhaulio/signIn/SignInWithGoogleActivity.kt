package com.example.codetestforhaulio.signIn

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.codetestforhaulio.R
import com.example.codetestforhaulio.jobs.JobsListActivity
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.SignInButton
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task


class SignInWithGoogleActivity : AppCompatActivity() {

    private var mGoogleSignInClient: GoogleSignInClient? = null
    private var signInButton: SignInButton? = null
    private val RC_SIGN_IN = 100
    private val TAG = "SignInWithGoogleActivit"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_with_google)
        initViews()

    }

    private fun initViews() {
        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build()

        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso)

        // Set the dimensions of the sign-in button.
        signInButton = findViewById(R.id.sign_in_button)
        signInButton!!.setSize(SignInButton.SIZE_WIDE)
        signInButton!!.setOnClickListener { signIn() }

    }

    override fun onStart() {
        super.onStart()
        // Check for existing Google Sign In account, if the user is already signed in
        // the GoogleSignInAccount will be non-null.
        val account = GoogleSignIn.getLastSignedInAccount(this)
        updateUI(account)
    }

    private fun signIn() {
        val signInIntent = mGoogleSignInClient!!.signInIntent
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSignInResult(task)
        }
    }

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account = completedTask.getResult(ApiException::class.java)
            // Signed in successfully, show authenticated UI.
            updateUI(account)
        } catch (e: ApiException) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.e(TAG, "signInResult:failed code=" + e.statusCode)
            Toast.makeText(this, e.message, Toast.LENGTH_SHORT).show()
        }

    }

    private fun updateUI(signInAccount: GoogleSignInAccount?) {
        if (null != signInAccount) {
            var givenName: String? = ""
            var photoUrl = ""
            if (null != signInAccount.givenName) {
                givenName = signInAccount.givenName
            } else if (null != signInAccount.displayName) {
                givenName = signInAccount.displayName
            }

            if (null != signInAccount.photoUrl) {
                photoUrl = signInAccount.photoUrl!!.toString()
            }
            Log.d(TAG, "updateUI: givenName$givenName")
            Log.d(TAG, "updateUI: photoUrl$photoUrl")


            val intent = Intent(this, JobsListActivity::class.java)
            intent.putExtra("name", givenName)
            intent.putExtra("photoUrl", photoUrl)
            startActivity(intent)
        }

    }

}
