package com.example.codetestforhaulio.data.network;

import com.example.codetestforhaulio.data.network.service.JobListService


class DataManager private constructor()// This class is not publicly instantiable
{

    val jobListService: JobListService
        get() = JobListService.getInstance()

    companion object {

        private var sInstance: DataManager? = null

        val instance: DataManager
            @Synchronized get() {
                if (sInstance == null) {
                    sInstance = DataManager()
                }
                return sInstance!!
            }
    }

}
