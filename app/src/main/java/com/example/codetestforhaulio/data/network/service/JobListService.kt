package com.example.codetestforhaulio.data.network.service

import JobListResponse
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

class JobListService private constructor() {

    val jobListApi: JobListApi

    init {
        val mRetrofit = Retrofit.Builder().addConverterFactory(GsonConverterFactory.create()).baseUrl(URL).build()
        jobListApi = mRetrofit.create(JobListApi::class.java)
    }

    interface JobListApi {
        @GET("8d195.json")
        fun getAllJobs(): Call<List<JobListResponse>>
    }

    companion object {
        private val URL = "https://api.myjson.com/bins/"
        private var instance: JobListService? = null

        fun getInstance(): JobListService {
            if (instance == null) {
                instance = JobListService()
            }
            return instance!!
        }
    }

}
