package com.example.codetestforhaulio.detail

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.SearchView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.codetestforhaulio.R
import com.example.codetestforhaulio.signIn.SignInWithGoogleActivity
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import de.hdodenhof.circleimageview.CircleImageView

class DetailsActivity : AppCompatActivity(), OnMapReadyCallback {


    private var mGoogleSignInClient: GoogleSignInClient? = null
    private var btn_sign_out: AppCompatImageView? = null
    private var edt_search: SearchView? = null
    private var tv_name: TextView? = null
    private var tv_job_no: TextView? = null
    private var iv_profile: CircleImageView? = null
    private var iv_back: AppCompatImageView? = null
    private var rl_map: RelativeLayout? = null
    private var lattitide: Double? = 0.0
    private var longtitude: Double? = 0.0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_job_detail)
        initViews()
    }

    private fun initViews() {
        btn_sign_out = findViewById(R.id.btn_sign_out)
        edt_search = findViewById(R.id.edt_search)
        tv_name = findViewById(R.id.tv_name)
        tv_job_no = findViewById(R.id.tv_job_no)
        iv_profile = findViewById(R.id.iv_profile)
        iv_back = findViewById(R.id.iv_back)
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map_view) as SupportMapFragment?
        mapFragment!!.getMapAsync(this)

        btn_sign_out!!.setOnClickListener { signOut() }

        val bundle = intent.extras
        if (null != bundle) {
            tv_name!!.text = bundle.getString("ussername")
            tv_job_no!!.text = "Job Number: " + bundle.getInt("job_id").toString()
            val url = bundle.getString("photoUrl")
            Glide.with(this@DetailsActivity).load(url).diskCacheStrategy(DiskCacheStrategy.ALL)
                    .dontAnimate().placeholder(R.drawable.ic_holder)
                    .error(R.drawable.ic_holder).into(iv_profile!!)
            lattitide = bundle.getDouble("lat")
            longtitude = bundle.getDouble("lng")
        }
        iv_back!!.setOnClickListener { onBackPressed() }

        edt_search!!.setOnQueryTextListener(object : SearchView.OnQueryTextListener {

            override fun onQueryTextChange(newText: String): Boolean {
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                // needs correct map api key to perform search
                return false
            }

        })
    }

    private fun signOut() {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build()
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso)
        mGoogleSignInClient!!.signOut()
                .addOnCompleteListener(this) {
                    startActivity(Intent(this, SignInWithGoogleActivity::class.java))
                }
    }

    // Google map api key may have some errors.
    override fun onMapReady(googleMap: GoogleMap?) {
        //replace the correct map key
        Log.d("DetailsActivity", "onMapReady: $lattitide")
        Log.d("DetailsActivity", "onMapReady: $longtitude")
        val latLng = LatLng(lattitide!!, longtitude!!)
        val map = googleMap
        map!!.getUiSettings().isMyLocationButtonEnabled = false
        val markerOptions = MarkerOptions()
        markerOptions.position(latLng)
        map.addMarker(markerOptions)

        map.addMarker(MarkerOptions()
                .position(latLng)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pin)))

        map.moveCamera(CameraUpdateFactory.newLatLng(latLng))
    }
}
