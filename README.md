Android Kotlin and MVVM Architecture Components sample
===================================

Using Technologies

- Kotlin
- Android Support Libraries
- Lifecycle-aware components
- MVVM Architecture (ViewModels,LiveData)
- Retrofit
- RxJava2
- Glide
- Gson
- Google Map

![](images/Screenshot_1.jpg)
![](images/Screenshot_2.jpg)
![](images/Screenshot_3.jpg)